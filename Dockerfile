FROM ubuntu

RUN apt-get install mercurial git bzr golang -y

RUN mkdir -p /usr/local/go

ENV GOPATH /usr/local/go

RUN go get github.com/spf13/hugo
RUN ln -s /usr/local/go/bin/hugo /usr/local/bin/

ADD . /root/docs

WORKDIR /root/docs

ENTRYPOINT ["hugo", "server", "-p", "80", "-b", "http://boost-app.net:88"]
