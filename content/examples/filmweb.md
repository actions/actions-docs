---
title: "Filmweb example "
linktitle: "Filmweb"
date: "2014-05-20"
weight: 20
author: "Łukasz Kurowski"
description: 
  Action which searches filmweb for a title and extracts movie details – title etc.
groups:
  - examples
menu:
  main:
    parent: 'examples'
---

This action searches filmweb for a title and extracts movie details like `title`, `original_title`, `description` and `poster`.

### Example result

    {
      "title": "Pulp Fiction",
      "poster": "http://1.fwcdn.pl/po/10/39/1039/7517880.3.jpg",
      "description": "Płatni mordercy, Jules (Samuel L. Jackson) i Vincent (...)"
    }

### Javascript

If you like JavaScript, you can use it to construct an action very easily and export it as JSON.

    var actions = require('actions');

    var filmweb = {};

    // Makes a http request to http://www.filmweb.pl/search?q={{title argument here}}
    filmweb.search = actions.construct([
      actions.construct('util.push', {$push: {title: 'query.q'}}),
      actions.construct('http.request', {url: 'http://www.filmweb.pl/search'}),
    ]);

    // Makes http request to filmweb movie
    // And xtracts movie datails from html page
    filmweb.extract = actions.construct([
      actions.construct('http.request', {hostname: 'filmweb.pl'}),
      actions.construct('html.extract', {
        selectors: {
          original_title: 'div.filmTitle h2',
          description: 'p.text',
          title: 'div.filmTitle h1',
          poster: {
            $extract: 'href',
            $path: 'div.posterLightbox a'
          }
        }
      })
    ]);

    // Makes a search on filmweb
    // Get's first result
    // And extracts it's details
    filmweb.find = actions.construct([
      'filmweb.search',
      actions.construct('html.extract', {
        selectors: {
          pathname: {
            $extract: 'href',
            $path: 'ul.resultsList li:first-child h3 a'
          }
        }
      }),
      'filmweb.extract'
    ]);

    actions.register('filmweb', filmweb);

    actions.run('filmweb.find', {title: 'Pulp Fiction'}).then(function(res) {
      console.log('Title:', res.title);
      console.log('Poster:', res.poster);
      console.log('Description:', res.description);
    });

### JSON

Action can be exported to json, then it can be imported or just runned, like this.

    {
      "name": "util.push",
      "args": {
        "$push": {
          "title": "query.q"
        }
      },
      "next": {
        "name": "http.request",
        "args": {
          "url": "http://www.filmweb.pl/search"
        },
        "next": {
          "name": "html.extract",
          "args": {
            "selectors": {
              "pathname": {
                "$extract": "href",
                "$path": "ul.resultsList li:first-child h3 a"
              }
            }
          },
          "next": {
            "name": "http.request",
            "args": {
              "hostname": "filmweb.pl"
            },
            "next": {
              "name": "html.extract",
              "args": {
                "selectors": {
                  "original_title": "div.filmTitle h2",
                  "description": "p.text",
                  "title": "div.filmTitle h1",
                  "poster": {
                    "$extract": "href",
                    "$path": "div.posterLightbox a"
                  }
                }
              }
            }
          }
        }
      }
    }
