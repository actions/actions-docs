---
title: "Google"
date: "2014-05-20"
weight: 0
author: "Łukasz Kurowski"
description:
  Action that looks in google for a phrase and extracts all results
groups:
  - examples
menu:
  main:
    parent: 'examples'
---

## Javascript

Javascript code constructing `google.find` action

    var actions = require('actions');

    var google = {};

    google.search = actions.construct('http.get', {url: 'https://www.google.com/search'});

    google.results = actions.construct('html.extract', {
      selectors: {
        results: {
          $path: 'li.g',
          $extract: [{
            title: 'h3',
            href: {
              $path: 'a',
              $extract: 'href'
            }
          }]
        }
      }
    });

    google.find = actions.construct([
      'google.search',
      'google.results'
    ]);

    actions.register('google', google);

    actions.run('google.find', {query: {q: phrase}}).then(function (response) {
      console.log('Results for', phrase);
      console.log(response.results)
    });

## JSON

`google.find` action constructed by javascript code above.

    {
      "name": "http.request",
      "args": {
        "method": "GET",
        "url": "https://www.google.com/search"
      },
      "next": {
        "name": "html.extract",
        "args": {
          "selectors": {
            "results": {
              "$path": "li.g",
              "$extract": [{
                "title": "h3",
                "href": {
                  "$path": "a",
                  "$extract": "href"
                }
              }]
            }
          }
        }
      }
    }

### Example result

Result of `google.find({phrase: "Go"})`

    [
      { title: 'The Go Programming Language', href: 'http://golang.org/' },
      { title: 'British Go Association: BGA Home Page', href: 'http://www.britgo.org/' },
      // 8 more...
    ]
