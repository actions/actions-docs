---
title: "Python"
linktitle: "Actions in Python"
date: "2014-05-20"
weight: 40
author: "Łukasz Kurowski"
menu:
  main:
    parent: 'developers'
groups:
  - developers
description:
  Running and constructing actions in Python.
---

### Example action

`google.find` - action which searchs in google for given phrase and crawls title and description of every result

    [
      ["util.push", {"$push": {"phrase": "query.q"}}],
      ["http.get", {"url": "https://www.google.com/search"}],
      ["html.extract", {
          "selectors": {
            "results": {
              "$path": "li.g",
              "$extract": [
                {
                  "title": "h3",
                  "href": {
                    "$path": "a",
                    "$extract": "href"
                  }
                }
              ]
            }
          }
        }
      ]
    ]

### Run action in Python

    import actions
    import json

    # register actions
    actions.register('google.find', json.loads(jsonAction))

    response = yield actions.run('google.find', phrase='Pulp Fiction')

