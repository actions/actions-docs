---
title: "JavaScript"
linktitle: "Actions in Javascript"
date: "2014-05-20"
weight: 30
author: "Łukasz Kurowski"
menu:
  main:
    parent: 'developers'
groups:
  - developers
description:
  Construct and execute actions right in the browser or on node.js.
---

### Example action

`google.find` - action which searchs in google for given phrase and crawls title and description of every result

    [
      ["util.push", {"$push": {"phrase": "query.q"}}],
      ["http.get", {"url": "https://www.google.com/search"}],
      ["html.extract", {
          "selectors": {
            "results": {
              "$path": "li.g",
              "$extract": [
                {
                  "title": "h3",
                  "href": {
                    "$path": "a",
                    "$extract": "href"
                  }
                }
              ]
            }
          }
        }
      ]
    ]

### Run action in Javascript

    var actions = require('actions');

    // register actions
    actions.register('google.find', JSON.parse(jsonAction));

    actions.patch(); //monkey patch

    var response = google.find({phrase: 'Pulp Fiction'});
