---
title: "Go"
linktitle: "Actions in Go"
date: "2014-05-20"
weight: 50
author: "Łukasz Kurowski"
menu:
  main:
    parent: 'developers'
groups:
  - developers
description:
  How to run and build actions in Go
---

### Example action

`google.find` - action which searchs in google for given phrase and crawls title and description of every result

    [
      ["util.push", {"$push": {"phrase": "query.q"}}],
      ["http.get", {"url": "https://www.google.com/search"}],
      ["html.extract", {
          "selectors": {
            "results": {
              "$path": "li.g",
              "$extract": [
                {
                  "title": "h3",
                  "href": {
                    "$path": "a",
                    "$extract": "href"
                  }
                }
              ]
            }
          }
        }
      ]
    ]

### Run action in Go

    import (
        "log"
        "github.com/crackcomm/actions.go"
    )

    func main() {
        actions.RegisterJSON("google.find", jsonAction)

        response, err := actions.Run("google.find", actions.Args{"phrase": "Pulp Fiction"})

        if err != nil {
          log.Fatalf("Error executing google.find: %v", err)
        }

        log.Println(<-response)
    }
