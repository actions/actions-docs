---
title: "Actions Cloud"
date: "2014-05-20"
weight: 30
author: "Łukasz Kurowski"
groups:
  - about
menu:
  main:
    parent: 'cloud'
description:
  Actions in the Cloud
---

### Export

This script exports `google.find` action to remote index so it can be used by other clients.

`google.find` - action which searchs in google for given phrase and crawls title and description of every result

    var actions = require('actions');

    // register an action index
    actions.remote('http://crackcomm.svc.io/my-project', {token: 'write-access-token'});

    var google = {};

    google.find = actions.construct([
      actions.construct('http.get', {url: 'http://www.google.com/search'}),
      actions.construct('html.extract', {selectors: {results: [{title: '.r', description: '.st'}]}})
    ]);

    // register actions
    actions.define('google', google);


Example result of `google.find({phrase: "Go"})`

```Javascript
{
  results: [
    { title: 'The Go Programming Language', description: '...' },
    { title: 'British Go Association: BGA Home Page', description: '...' },
    // 8 more...
  ]
}
```

### Import

Now you can execute `google.find` on your computer or in the cloud. It was registered on `http://crackcomm.svc.io/my-project` so you can just grab it.

```Javascript
var actions = require('actions');

actions.remote('http://crackcomm.svc.io/my-project');

var response = actions.run('google.find', {phrase: 'Pulp fiction'});
// or
actions.patch(); // monkey patch
var response = google.find({phrase: 'Pulp fiction'});
```

### Actions in the cloud

Every action is just a tiny documents, nothing else.

This is how `google.find` action exists in the cloud.

```yaml
-
  name: http.get
  args:
    url: http://www.google.com/search
-
  name: html.extract
  args:
    selectors:
      results:
        -
            title:       .r
            description: .st
```

We have created `google` collection containing action `find`, this is how it was saved in the cloud.

### Actions routes

Now we can use `google.find` action and serve the results directly to clients.

Lets create a simple webpage - a `goodle.com`.

All we have to do is define the routes

    GET  /             -            html  "index.html"
    GET  /search       google.find  html  "results.html"

A simple template in `results.html`

    {% for result in results %}
      <h3>{{ result.title }}</h3>
      <p>{{ result.description }}</p>
    {% endfor %}

And its done!

Now you can search on `http://goodle.com`.

For example. `http://goodle.com/search?phrase=Pulp+Fiction`
