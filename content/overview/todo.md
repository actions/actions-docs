---
title: "TODO"
date: "2014-03-20"
weight: 15
author: "Spencer Lyon"
menu:
  main:
    parent: 'todo'
description:
  Prioritized TODO list
---

> docs:
> 
> * actions core specs

1. Actions Store (`http api` &#8594; `database`)
2. Actions Store Frontend (`html app` &#8594; `store api`)
3. Actions Cloud (`store` + `running in cloud`)
4. Javascript Store Library (`actions in browser`)
5. Actions Designer (`html app` &#8594; `store api`)

#### Actions Store

HTTP API for storing actions in the cloud.

    GET     /{user}         actions.all     json
    GET     /{user}/{name}  actions.all     json
    PUT     /{user}/{name}  actions.add     json
    POST    /{user}/{name}  actions.add     json
    DELETE  /{user}/{name}  actions.delete  json
