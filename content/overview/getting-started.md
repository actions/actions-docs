---
title: "Getting Started"
date: "2014-03-20"
weight: 1
author: "Spencer Lyon"
menu:
  main:
    parent: 'overview'
description:
  Look – its really easy
---

### Basics

Action is a simple chain of function calls.
Let me give you an example in JavaScript what chain of function calls is.

#### Javascript chain example

To extract a page's `title` using a *JavaScript* code we would write something like this

    var getTitle = function (url, callback) {
      var selectors = {title: 'title'};
      http.request(url, function(response, body) {
        var result = html.extract(selectors, body);
        callback(result);
      });
    };

    getTitle("http://www.google.com/", function (result) {
      console.log("title:", result.title);
    });

It's just a chain which calls two functions.

First – `http.request` and next – `html.extract`.

It seems like a really simple data, lets write it down as JSON.

#### Plain text chain example

    [
      ['http.request', {url: 'http://www.google.pl'}],
      ['html.extract', {title: 'title'}]
    ]

#### JSON chain example

We will use `name` parameter to represent function name.
Arguments are under `args` parameter.
Under `next` parameter there is a next `function`.

    {
      "name": "http.request",
      "args": {
        "url": "http://www.google.com",
      },
      "next": {
        "name": "html.extract",
        "args": {
          "title": "title"
        }
      }
    }

Or even simpler:

    [
      ['http.request', {url: 'http://www.google.pl'}],
      ['html.extract', {title: 'title'}]
    ]

Can you believe this is `action` ? All about this small piece of `JSON`.

You can now run `JSON` above and it will extract `title` from the html page and return:

    "Google"

#### How to use

If you want to know [how to use](/overview/how-to-use/) actions, keep reading.
