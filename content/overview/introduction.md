---
title: "Introduction"
date: "2014-03-20"
weight: 0
author: "Spencer Lyon"
menu:
  main:
    parent: 'overview'
groups:
  - about
description:
  What is it all about
---

### What is Action ?

Before anwering is question it will be better if we will say what `programming` is.

#### Programming

Programming in simple words is combining appropriate function calls and achieving expected results.

#### Actions

Actions in this concept are exactly the same. They are combined function calls which achieve the same results.

#### Difference

Difference between making `actions` and `programming` is you do not need to know a programming language to create a `function`.

#### Why ?

Because action is just abstract way to chain function calls.

Actions can be designed in [Graphical User Interface](/???) and exported as `JSON`.

Created action can be then executed – in the [cloud](/cloud/about) or in [your program](/interpreters/).

#### Tell me more

If you are interested in this game – [get started](/overview/getting-started).
