---
title: "How to Use"
date: "2014-05-20"
weight: 2
author: "Łukasz Kurowski"
menu:
  main:
    parent: 'overview'
description:
  Tool is useful only when used appropriate
---

### Basic Usage

Actions can be executed in the [cloud](/cloud/about/) on [desktop](/TODO) or on [mobile](/mobile/).

### Development

We have built few tools to help you develop some actions – they are available in the [browser](?) and on [mobile](?).

### Programming

If you are a programmer you can use actions across different [programming languages](/developers/).

