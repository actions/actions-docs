---
title: "Running on Android"
date: "2014-05-20"
weight: 60
author: "Łukasz Kurowski"
menu:
  main:
    parent: 'mobile'
description:
  Android application for building, storing and running actions.
---
