---
title: "Core"
linktitle: "Actions core"
date: "2014-05-20"
weight: 10
author: "Łukasz Kurowski"
groups:
  - about
menu:
  main:
    parent: 'core'
description:
  The brain of every action
---

### Code

Actions are not really working without any code, they are using `functions` that are defined in a *core* (or an *interpreter*).
And you don't have to care about them, you can just [use them](/overview/how-to-use/).

### Consistency

Consistency of every `function` in different cores is critical when it comes to execution across various `platforms` or `programming languages`.
Compability of core can be achieved by following the `core standard`.

### Standard

Standard is defined in [actions core specification](/core/specs/) which is also a [documentation](/core/specs/).

### Testing

Actions core can be tested based on `specification` thanks to [core tester](/core/tester/).
