---
title: "Core Tester"
date: "2014-05-20"
weight: 20
author: "Łukasz Kurowski"
description:
  Keep it fresh
---

### Tester

If you want to develop a new core you can use our [tester](http://tester.actions.io/)
