---
title: "Core specification"
date: "2014-05-20"
weight: 20
author: "Łukasz Kurowski"
menu:
  main:
    parent: 'core'
description:
  Core documentation
---

### Documentation

Core documentation is a data that you can explore in different formats.

You can find it as [documentation page](http://docs.acions.io) or pure [JSON data](http://docs.acions.io/?format=json).

### Specification

Core specification defines few most important things that describe `function`.

For example specification of `log.write` function.

    {
      "name": "log.write",
      "description": "Writes to log file",
      "arguments": {
        "level": {
          "type": "string",
          "description": "Log level",
          "default": "INFO",
          "example": ["DEBUG", "INFO", "ERROR"]
        },
        "body": {
          "type": "string",
          "required": true,
          "description": "Log body"
        }
      },
      "returns": {
        "success": {
          "type": "bool",
          "description": "Was it success?"
        }
      }
    }

Above document specifies a description of function and few more important things that describes function:

1. It's name (`name`)
2. Arguments – including `example` and `default` (`arguments`)
3. Results (`returns`)

Based on that we can generate [documentation](http://docs.acions.io).
